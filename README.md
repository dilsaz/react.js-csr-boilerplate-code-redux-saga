# react.js


## direct app start

- **(install > test > build > start)**
- **default port : 80**

```bash
yarn app:start
```

---

## Used Technologies

- React.js (Hooks)
- Sass (.scss)
- jest-cucumber (BDD)
- Storybook
- Redux
- Redux-Saga
- Redux DevTools (for dev)
- Immutable.js (for redux)
- Web storage (localStorage with redux)
- Webpack (4)
- Node.js
- nodemon
- Babel (JavaScript compiler)
- Prettier (Code Formatter)
- husky (Git hooks)
- Docker
- GitLab CI/CD


---

## for development

- **first server start**

```bash
yarn start:dev
```

- **for client app**

_open a second terminal._

- auto starting
  - DevServer
  - Hot Module Replacement (HMR)

```bash
cd ./client
yarn start
```

---

## prod build

```bash
cd ./client
yarn build
cd ../
yarn start
```

---

## client app test

```bash
yarn client:test
```

---

## Docker

_for docker build_

```bash
docker build -t react-js-boilerplate-client .
```

_after start command_

```bash
docker run -it -p 80:80 react-js-boilerplate-client
```

_Started at "http://localhost:80"_

