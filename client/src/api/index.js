import axios from "axios";

axios.defaults.validateStatus = (status) => {
  return status >= 200 && status < 400;
};
axios.interceptors.response.use(
  (response) => {
    return response;
  },
  function (error) {
    /*
    if (error.response.status === 401) {
      if (
        window.location.pathname === "/login" ||
        error.config.url.includes("login")
      ) {
        alert(
          "Kullanıcı adı veya şifre hatalı!\nLütfen bilgilerinizi kontrol ederek tekrar deneyiniz."
        );
        window.location.reload();
      } else {
        if (window.document.getElementById("logout-hide-button")) {
          window.document.getElementById("logout-hide-button").click();
        } else {
          window.location = "/login";
        }
      }
    }
  */
    // return Error object with Promise
    return Promise.reject(error);
  }
);

class API {
  constructor() {
    this.name = "API";
  }
  fetch(param) {
    let { url, method, data } = param;
    return new Promise((resolve, reject) => {
      axios({
        method: method,
        url: url,
        data: data,
      })
        .then((response) => {
          if (response.data) {
            resolve(response.data);
          } else {
            reject(response);
          }
        })
        .catch((err) => {
          reject(err);
        });
    });
  }
}
export default new API();
