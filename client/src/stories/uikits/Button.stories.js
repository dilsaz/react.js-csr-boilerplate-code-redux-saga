import React from "react";
import { action } from "@storybook/addon-actions";
//import { iconList, colorList } from "@uiVariables";
const Comp = (props) => <button {...props}>storybook test buton</button>;
export default {
  title: "Uikit/Button",
  component: Comp,
};

const Template = (args) => (
  <div>
    <Comp {...args} />
  </div>
);

export const Default = Template.bind({});
Default.args = {
  value: "Button",
  onClick: action("click"),
};
