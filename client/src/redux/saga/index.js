import api from "@api";
import { put, takeLatest, all, call, select } from "redux-saga/effects";
import * as actionTypes from "@actionTypes";
// import * as actions from "@actions";
class SagaAsyncInterface {
  constructor() {
    this.name = "SagaAsyncInterface";
  }

  *testData(action) {
    // const state = yield select();
    // const store = state.toJS();

    const fetchTest = () => {
      return api
        .fetch({
          url: "/api/test",
          method: "POST",
          data: {
            type: action.payload,
          },
        })
        .then((res) => res)
        .catch((error) => error.response.data);
    };

    const response = yield call(fetchTest);

    yield put({
      type: actionTypes.UPDATE_TEST_DATA,
      payload: response,
    });
  }
}

const SagaInterface = new SagaAsyncInterface();
export default function* RootSagaAsyncInterface() {
  yield all([takeLatest(actionTypes.LOAD_TEST_DATA, SagaInterface.testData)]);
}
