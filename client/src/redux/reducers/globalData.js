import React from "react";
import { createReducer } from "redux-immutablejs";
import Immutable from "immutable";
import * as actionTypes from "@actionTypes";

const initialState = {
  load: false,
  loading: false,
  modal: {
    status: false,
    onClose: () => {},
    isDialog: false,
    title: "",
    body: <div></div>,
  },
  test: "",
  productTypeCode: "MOTOR_VEHICLES",
};

const initialData = Immutable.fromJS(initialState);

export default createReducer(initialData, {
  [actionTypes.CHANGE_LOAD_STATUS]: (state, action) =>
    state.merge({ load: action.payload }),

  [actionTypes.OPEN_MODAL]: (state, action) =>
    state.merge({ modal: action.payload }),

  [actionTypes.CLOSE_MODAL]: (state, action) =>
    state.merge({ modal: action.payload }),

  [actionTypes.UPDATE_TEST_DATA]: (state, action) =>
    state.merge({ test: action.payload }),

  [actionTypes.UPDATE_LOADING]: (state, action) =>
    state.merge({ loading: action.payload }),
});
