import { combineReducers } from "redux-immutablejs";
import globalData from "./globalData";

export default combineReducers({
  globalData: globalData,
});
