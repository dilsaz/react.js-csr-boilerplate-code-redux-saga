import * as actionTypes from "@actionTypes";

export const openModal = (payload) => ({
  type: actionTypes.OPEN_MODAL,
  payload: {
    ...payload,
    status: true,
    isDialog: false,
  },
});

export const openDialog = (payload) => ({
  type: actionTypes.OPEN_MODAL,
  payload: {
    ...payload,
    status: true,
    isDialog: true,
  },
});

export const closeModal = () => ({
  type: actionTypes.CLOSE_MODAL,
  payload: { status: false },
});

export const loadTestData = (payload) => ({
  type: actionTypes.LOAD_TEST_DATA,
  payload: payload,
});

export const updateTestData = (payload) => ({
  type: actionTypes.UPDATE_TEST_DATA,
  payload: payload,
});

export const openLoading = () => ({
  type: actionTypes.UPDATE_LOADING,
  payload: true,
});

export const closeLoading = () => ({
  type: actionTypes.UPDATE_LOADING,
  payload: false,
});
