import { createStore, applyMiddleware } from "redux";
import createSagaMiddleware from "redux-saga";
import * as storage from "redux-storage";
import createEngine from "redux-storage-engine-localstorage";
import { composeWithDevTools } from "redux-devtools-extension";
//import { composeWithDevTools }        from 'redux-devtools-extension/developmentOnly';

import appReducers from "@appReducers";
import appSaga from "@appSaga";

const sagaMiddleware = createSagaMiddleware();
const appStateName = "test_project";
const engine = createEngine(appStateName);
const storageMiddleware = storage.createMiddleware(engine);
const stateMerger = (oldState, newState) => oldState.merge(newState);
const isTestMode = !!(
  ((window || {}).navigator || {}).userAgent || ""
).includes("jsdom");
class AppStoreData {
  start() {
    const reducer = storage.reducer(appReducers, stateMerger);
    const store = createStore(
      reducer,
      composeWithDevTools(applyMiddleware(sagaMiddleware, storageMiddleware))
    );

    sagaMiddleware.run(appSaga);

    const load = storage.createLoader(engine);

    // Do not use web storage in the test environment.
    if (!isTestMode) load(store);

    return store;
  }
}

export default new AppStoreData();
