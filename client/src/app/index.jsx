import React from "react";
import Layout from "@layout";
import TestPage from "@page.testPage";
import { useSelector, useDispatch } from "react-redux";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
// import * as actions from "@actions";

/*
const stateSelector = (state) => ({
  oauth: state.get("oauth").toJS(),
});
*/

const App = () => {
  const dispatch = useDispatch();
  // const stateData = useSelector(stateSelector);
  // const isLogin = stateData.oauth.isLogin;
  const isLogin = false;

  return (
    <Router>
      <Switch>
        <Route exact path={["/test-page", "/test-page/:page"]}>
          {isLogin ? <TestPage /> : <TestPage />}
        </Route>
        <Route exact path="/login">
          <TestPage />
        </Route>
        <Route exact path="*">
          {isLogin ? (
            <Layout title={"Test Page"}>
              <TestPage />
            </Layout>
          ) : (
            <TestPage />
          )}
        </Route>
      </Switch>
      {/*
     <button
     style={{ display: "none" }}
     id="logout-hide-button"
     onClick={() => dispatch(actions.userLogout())}
   />
   */}
    </Router>
  );
};

export default App;
