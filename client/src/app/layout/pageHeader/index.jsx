import React from "react";
import PropTypes from "prop-types";
import backIcon from "@image.arrow-ios-left.svg";
import editIcon from "@image.icons-edit.svg";
import addIcon from "@image.icons-add.svg";
import saveIcon from "@image.icons-save.svg";

const rightActionButtons = (p) => {
  let buttons = [];
  if (p.onClickSave)
    buttons.push(
      <img
        onClick={p.onClickSave}
        src={saveIcon}
        alt="Save"
        className="action-button"
        key="save"
      />
    );
  if (p.onClickEdit)
    buttons.push(
      <img
        onClick={p.onClickEdit}
        src={editIcon}
        alt="Edit"
        className="action-button"
        key="edit"
      />
    );
  if (p.onClickAdd)
    buttons.push(
      <img
        onClick={p.onClickAdd}
        src={addIcon}
        alt="Add"
        className="action-button"
        key="add"
      />
    );
  return buttons;
};

const PageHeader = (props) => {
  return (
    <div className="page-header">
      <div className="left-content">
        <img
          src={backIcon}
          onClick={props.onClickBack}
          alt=""
          className="action-button"
        />
      </div>
      <div className="center-content">{props.title}</div>
      <div className="right-content">{rightActionButtons(props)}</div>
    </div>
  );
};

PageHeader.propTypes = {
  title: PropTypes.string.isRequired,
  onClickBack: PropTypes.func,
  onClickSave: PropTypes.func,
  onClickEdit: PropTypes.func,
  onClickAdd: PropTypes.func,
};

PageHeader.defaultProps = {
  onClickBack: () => {},
};

export default PageHeader;
