import React from "react";
import PropTypes from "prop-types";
import Header from "@layout.header";
import Main from "@layout.main";

const Layout = (props) => {
  return (
    <React.Fragment>
      <Header title={props.title} />
      <Main>{props.children}</Main>
    </React.Fragment>
  );
};

Layout.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]),
};

export default Layout;
