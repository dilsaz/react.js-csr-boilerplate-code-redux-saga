import React, { useRef, useEffect } from "react";
import { useSelector, useDispatch } from "react-redux";
import { Button, Box } from "@material-ui/core";

//import * as actions from "@actions";
/*
const stateSelector = (state) => ({
  oauth: state.get("oauth").toJS(),
  globalData: state.get("globalData").toJS(),
});
*/
export default () => {
  const dispatch = useDispatch();
  /*
  const stateData = useSelector(stateSelector);
  const profile = stateData.oauth.profile;
  const globalData = stateData.globalData;
  */

  const mounted = useRef();
  useEffect(() => {
    if (!mounted.current) {
      // do componentDidMount logic
      mounted.current = true;
    } else {
      // do componentDidUpdate logic
    }
  });

  return (
    <Box
      color="text.primary"
      display="grid"
      textAlign="center"
      justifyContent="center"
      m={1}
      p={1}
      bgcolor="background.paper"
    >
      <h1 className="m-b-15">Welcome</h1>
      <Button color="primary">Hello World</Button>
    </Box>
  );
};
