import React from "react";
import { useSelector } from "react-redux";

const stateSelector = (state) => ({
  loading: state.getIn(["globalData", "loading"]),
});

export default () => {
  const stateData = useSelector(stateSelector);
  if (!stateData.loading) return null;
  return (
    <div className="app-splash-screen active">
      <div className="lds-spinner">
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
        <div></div>
      </div>
      <h5>loading</h5>
    </div>
  );
};
