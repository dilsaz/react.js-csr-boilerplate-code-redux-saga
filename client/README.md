# Online TTP - oto yıkama - Client App

[project detail](https://gitlab.com/online-ttp/front-end/product)

## development start for client app

- _before server start_

```bash
yarn start
```

---

## prod build

```bash
yarn build
```

---

## test

```bash
yarn test
```

**for test watch**

```bash
yarn test:watch
```

---

## License

[MIT](https://choosealicense.com/licenses/mit/)
