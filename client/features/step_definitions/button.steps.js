import { defineFeature, loadFeature } from "jest-cucumber";
import { render, fireEvent } from "@testing-library/react";
import React from "react";
import * as customFunctions from "@customFunctions";

const feature = loadFeature("./features/scenarios/button.feature");
let wrapper = null;

defineFeature(feature, (test) => {
  test("buton click", ({ given, when, then }) => {
    given(/^buton click$/, () => {});

    when(/^I click button$/, (arg0) => {});

    then(/^onclick event button$/, (arg0) => {});
  });
});
