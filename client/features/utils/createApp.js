import React from "react";
import { Provider } from "react-redux";
import { render } from "@testing-library/react";
import App from "@app";
import appStoreData from "@appStoreData";

export default () => {
  const store = appStoreData.start();
  const Comp = render(
    <Provider store={store}>
      <App />
    </Provider>
  );
  return {
    Comp: Comp,
    store: store,
  };
};
