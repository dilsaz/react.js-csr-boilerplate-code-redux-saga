FROM node:latest

WORKDIR /

COPY ./ ./app

RUN cd ./app/client \
    && yarn \
    && yarn test \
    && yarn build \
    && yarn storybook:build \
    && cd ../ \
    && yarn

EXPOSE 80
CMD ["node", "./app/index.js"]