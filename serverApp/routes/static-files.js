const path = require("path");
const fs = require("fs");
const featuresPath = "../../client/features/scenarios/";

module.exports = (req, res, next) => {
  switch (req._parsedUrl.pathname) {
    case "/test":
      res.sendFile(
        path.join(__dirname + "../../../client/test-report/index.html")
      );
      break;

    case "/features":
      staticTestFiles(req, res);
      break;

    default:
      next();
      break;
  }
};

const staticTestFiles = (req, res) => {
  if (req.query.file) {
    try {
      const file = fs.readFileSync(
        path.join(__dirname, featuresPath + req.query.file),
        "utf8"
      );
      res.send(`<pre>${file}</pre>`);
    } catch (_error) {
      res.sendStatus(404);
    }
  } else {
    let fileList = [];
    fs.readdirSync(path.join(__dirname, featuresPath)).forEach((file) => {
      fileList.push(
        `<li><a href="/features?file=${file}" target="_blank">${file}</a></li>`
      );
    });
    res.send(`<ul>${fileList.join("")}</ul>`);
  }
};
