const express = require("express");
const controller = require("../controller/index");
const expressRouter = express.Router();

controller(expressRouter);

module.exports = expressRouter;
