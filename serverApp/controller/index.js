const serviceFunc = require("../utils/service-func");

const testContoller = require("./test/index");

module.exports = (router) => {
  // authorizationController eklenecek.
  testContoller(router);
  // refresh token error handler eklenecek.
  router
    .route("/api/*")
    .all((req, res, next) => serviceFunc.errorMethodHandler(req, res, next));
};
