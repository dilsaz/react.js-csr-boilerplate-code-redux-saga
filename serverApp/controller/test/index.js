const domain = require("../../domain/index");
// const serviceFunc = require("../../utils/service-func");

const Test = (router) => {
  router
    .route("/api/test")
    .post((req, res, next) => {
      domain.test
        .testApi(req.body)
        .then((response) => {
          res.json(response);
        })
        .catch((error) => next(error));
    })
  };

module.exports = Test;
