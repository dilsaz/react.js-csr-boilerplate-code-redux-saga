class Test {
  constructor() {}

  testApi(data) {
    return new Promise((resolve, _reject) => {
      resolve({ data : data });
    });
  }
}

module.exports = new Test();
