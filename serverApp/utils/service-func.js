exports.errorMethodHandler = (req, res, next) => {
  next({
    scope: "server",
    status: 405,
    error: "405 Method Not Allowed",
  });
};

exports.routerErrorHandler = (error, req, res, next) => {
  let resStatus = error.status;
  if (!error.status)
    resStatus = JSON.stringify(error).match(/unauthorized/gi) ? 401 : 500;
  res.status(resStatus);
  res.json(error);
};
