const axios = require("axios");
var apiURL = "http://ip.jsontest.com";

axios.defaults.validateStatus = (status) => {
  return status >= 200 && status < 400;
};

axios.interceptors.response.use(
  (response) => {
    return response;
  },
  function (error) {
    /*
    if (error.response.status === 401) {
      //refresh token...
    }
    */
    // return Error object with Promise
    return Promise.reject(error);
  }
);

class API {
  constructor() {}

  fetch(requestConfig) {
    let { url, method, data, token } = requestConfig;
    return new Promise((resolve, reject) => {
      const axiosConfig = {
        method: method || "GET",
        url: apiURL + url,
        data: data || null,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      };

      let finallyData = {
        request: Object.assign({}, axiosConfig),
        results: {},
      };
      delete finallyData.request.headers;

      axios(axiosConfig)
        .then((response) => {
          if (response.data) {
            finallyData.results = {
              fStatus: `then response data - ${response.status}`,
              fData: response.data,
            };
            resolve(response.data);
          } else {
            finallyData.results = {
              fStatus: `then response - ${response.status}`,
              fData: response,
            };
            reject(response);
          }
        })
        .catch((err) => {
          const eData = {
            status: err.response.status,
            data: err.response.data,
          };
          finallyData.results = {
            fStatus: `catch response - ${eData.status}`,
            fData: eData.data,
          };
          reject(eData);
        })
        .finally(() => {
          try {
            console.log(JSON.stringify(finallyData));
          } catch (error) {
            console.log("finally error parse");
          }
        });
    });
  }
}
module.exports = new API();
