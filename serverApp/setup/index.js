const express = require("express");
const bodyParser = require("body-parser");
const session = require("express-session");
const path = require("path");

let globalData = require("../utils/global-data");
const serverAppRoutes = require("../routes/index");
const staticFilesMiddleware = require("../routes/static-files");
const serviceFunc = require("../utils/service-func");
class Setup {
  constructor() {
    this.app = express();
  }

  createServer() {
    this.app.use(bodyParser.json());
    const memoryStore = new session.MemoryStore();
    this.app.use(
      session({
        secret: "D16A2731D89F5AC371CFAEB2CF793",
        resave: false,
        saveUninitialized: true,
        store: memoryStore,
      })
    );

    this.app.use("/", serverAppRoutes);
    this.app.use(staticFilesMiddleware);
    this.app.use(serviceFunc.routerErrorHandler);
    this.app.use(
      "/storybook",
      express.static(path.join(__dirname, "../../client/storybook-static"))
    );
    this.app.use(express.static(path.join(__dirname, "../../client/dist")));
    this.app.all("*", (_req, res) => {
      res.sendFile("client/dist/index.html", {
        root: path.join(__dirname, "../../"),
      });
    });
  }

  startServer() {
    const serverAppPORT = process.env.PORT || 80;
    const serverAppLine = this.app.listen(serverAppPORT, () => {
      const params = serverAppLine.address();
      const host = params.address === "::" ? "localhost" : params.address;
      const port = params.port;
      console.log("app server listening at %s:%s", host, port);
    });
  }
}
module.exports = new Setup();
